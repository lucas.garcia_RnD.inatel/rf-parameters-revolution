import os

from Connections import conecta_mariadb


def executeScriptsFromFile(filename):
    #DEFINE CONEXÃO DB
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()
    # Open and read the file as a single buffer
    fd = open(filename, 'r')
    sqlFile = fd.read()
    fd.close()

    # all SQL commands (split on ';')
    sqlCommands = sqlFile.split(';')

    # Execute every command from the input file
    for command in sqlCommands:
        # This will skip and report errors
        # For example, if the tables do not yet exist, this will skip over
        # the DROP TABLE commands
        try:
            cursor.execute(command)
            con_bacos.commit()
        except Exception as e:
            print(f'Command skipped: {e}')

def audit():
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "SQL"))
    script_path = os.path.abspath(os.path.join(directory_path, 'scripts'))
    all_query_files = os.listdir(script_path)

    for query in all_query_files:
        actual_query = query.replace('.sql','')
        actual_query_file = os.path.join(script_path, query)
        print(f'Runnig: {actual_query}')
        executeScriptsFromFile(actual_query_file)
