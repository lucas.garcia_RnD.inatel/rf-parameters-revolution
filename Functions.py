import os
import platform

from Connections import conecta_mariadb
import pandas as pd
from datetime import datetime

def clean_csv_folders():
    print("Cleaning CSV directories...")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    states = ['SP','CO','PRSC','RS','RJ','ES','MG','NE']

    for state in states:
        state_path = os.path.join(csv_path, state)
        all_tables = os.listdir(state_path)
        for table in all_tables:
            csv_final_path = os.path.join(state_path, table)
            os.remove(csv_final_path)
    print("Finish!")

def compare_df():
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    states = ['SP','CO','PRSC','RS','RJ','ES','MG','NE']
    tec = ['RF_LTE','RF_GSM','RF_WCDMA','RF_NR']
    column_list = []

    for t in tec:
        general_dict = {}
        for state in states:

            #column_list.append(f'{state}__{t}')
            state_path = os.path.join(csv_path, state)
            all_tables = os.listdir(state_path)
            for table in all_tables:
                if t in table.upper():
                    csv_final_path = os.path.join(state_path, table)
                    df_csv = pd.read_csv(csv_final_path, dtype = 'str', low_memory=False)
                    #column_list.append(list(df_csv.columns))
                    general_dict[f'{state}_{t}'] = list(df_csv.columns)

        #df = pd.DataFrame (column_list)
        df = pd.DataFrame.from_dict(general_dict,orient='columns')
        df.to_csv(f'{csv_path}/columns_{t}.csv', index = False, header=True)
    x = 90

def convert_xls_csv():
    #limpando
    clean_csv_folders()
    #definindo paths
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    xls_path = os.path.abspath(os.path.join(directory_path, 'xls'))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    states = ['SP','CO','PRSC','RS','RJ','ES','MG','NE']
    #states = ['SP']
    sheets = ['RF_LTE','RF_GSM','RF_WCDMA','RF_NR']
    column_dict = {
        'RF_LTE':'A:BX',
        'RF_GSM':'A:CI',
        'RF_WCDMA':'A:BM',
        'RF_NR':'A:CA'
    }
    #columns to format
    format_columns = ['SIT_NAME','CITY','ADDRESS','NEIGHBORHOOD']

    #listando arquivos xls
    all_tables = os.listdir(xls_path)

    #definindo data pra stamp
    current_date = datetime.now()
    current_date = current_date.strftime('%Y-%m-%d')

    #loop processa RF parameters
    for table in all_tables:
        table_name = table.replace('.xlsb','')
        table_name = table_name.replace(' - ','_')
        #DEFINE ESTADO
        for state in states:
            if f'_{state}' in table_name.upper():
                print(f'=====>Processing {state}<=====')
                current_mo_dir = f'{xls_path}\\{table}'
                for sheet in sheets:
                    try:
                        read_file = pd.read_excel(current_mo_dir, engine='pyxlsb', dtype='str', sheet_name=sheet, usecols=column_dict[sheet])
                        #deleta linha de indices das params
                        read_file = read_file.drop(0)
                        #isere colunas da regiao e data
                        read_file.insert(0,'REG', f'{state}', allow_duplicates=True)
                        read_file.insert(0,'DATE', f'{current_date}', allow_duplicates=True)
                        #remove accents set space to _ and set upper (colunas listadas acima)
                        for column in format_columns:
                            read_file[column] = read_file[column].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
                            read_file[column] = read_file[column].replace('  ', ' ', regex=True)
                            read_file[column] = read_file[column].replace(' ', '_', regex=True)
                            read_file[column] = read_file[column].str.upper()
                        #salvando csv
                        read_file.to_csv (f'{csv_path}/{state}/{sheet}.csv', index = None, header=True)
                        print(f'Done {sheet}.')
                    except:
                        continue
            else:
                continue

def create_db():
    # criando cursor do bd
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    tec = ['RF_LTE','RF_GSM','RF_WCDMA','RF_NR']

    #criando db
    sql = f'drop database if exists huawei_ept;'
    cursor.execute(sql)
    con_bacos.commit()
    sql = f'create database if not exists huawei_ept;'
    cursor.execute(sql)
    con_bacos.commit()
    for t in tec:
        state_path = os.path.join(csv_path, 'SP')
        all_tables = os.listdir(state_path)
        for table in all_tables:
            if t in table.upper():
                csv_final_path = os.path.join(state_path, table)
                df_csv = pd.read_csv(csv_final_path, dtype = 'str', low_memory=False)
                list_colums = list(df_csv.columns)
                #criando tabela se o comando for novo
                sql_table_creation = f'CREATE TABLE IF NOT EXISTS huawei_ept.{t} ('
                for column in list_colums:
                    if not column == 'DATE':
                        column = column.replace('.','')
                        column = column.replace('/','_')
                        column = column.replace('-','_')
                        column = column.replace('(','')
                        column = column.replace(')','')
                        column = column.replace('ObservaÃ§Ãµes','Observacoes')
                        if column == 'Observações':
                            sql_insert_col = f"{column.replace(' ','_')} varchar(500) DEFAULT '-', "
                        elif column == 'ADDRESS':
                            sql_insert_col = f"{column.replace(' ','_')} varchar(200) DEFAULT '-', "
                        elif column == 'OBS':
                            sql_insert_col = f"{column.replace(' ','_')} varchar(500) DEFAULT '-', "
                        else:
                            sql_insert_col = f"{column.replace(' ','_')} varchar(100) DEFAULT '-', "
                        sql_table_creation = sql_table_creation + sql_insert_col
                    else:
                        column = column.replace('.','')
                        column = column.replace('-','_')
                        sql_insert_col = f"{column.replace(' ','_')} date DEFAULT NULL, "
                        sql_table_creation = sql_table_creation + sql_insert_col

                sql_table_creation = sql_table_creation[:-2] + ');'
                cursor.execute(sql_table_creation)
                con_bacos.commit()
    con_bacos.close()

def upload_csv(Reset_baco):
    # criando cursor do bd
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    states = ['SP','CO','PRSC','RS','RJ','ES','MG','NE']
    tec = ['RF_LTE','RF_GSM','RF_WCDMA','RF_NR']
    audits = ['AUDIT_DIVERGENTES','AUDIT_DUPLICADOS']

    #limpando bacos
    if Reset_baco:
        print('Cleaning DB...')
        for t in tec:
            sql = f'TRUNCATE huawei_ept.{t}'
            cursor.execute(sql)
            con_bacos.commit()
        for audit in audits:
            sql = f'TRUNCATE huawei_ept.{audit}'
            cursor.execute(sql)
            con_bacos.commit()
        print('Cleaning DB - OK')

    for t in tec:
        print(f'Loading {t} tables...')
        for state in states:
            state_path = os.path.join(csv_path, state)
            all_tables = os.listdir(state_path)
            for table in all_tables:
                if t in table.upper():
                    #subindo CSV na tabela
                    csv_final_path = os.path.join(state_path, table)
                    csv_final_path = csv_final_path.replace('\\', '/')

                    cur_platform = platform.system()
                    if cur_platform.upper() == 'WINDOWS':
                        sql = f'LOAD DATA LOCAL INFILE "{csv_final_path}" REPLACE INTO TABLE huawei_ept.{t}\n' \
                              f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                              f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
                    else:
                        sql = f'LOAD DATA LOCAL INFILE "{csv_final_path}" REPLACE INTO TABLE huawei_ept.{t}\n' \
                              f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                              f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
                    cursor.execute(sql)
                    con_bacos.commit()

                    print(f'Done: {state}_{table}')
                    #os.remove(csv_final_path)

    con_bacos.close()
