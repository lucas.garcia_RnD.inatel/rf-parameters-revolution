from Audits_runner import audit
from datetime import datetime
from Functions import  compare_df, convert_xls_csv, create_db, upload_csv

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    start = datetime.now()

    #configs
    Convert_ept_to_csv = True
    Compare_ept_columns = False
    CreateDb = False
    Upload_csv_to_baco = True
    Reset_baco = True #define se trunca as tabelas do baco antes de subir novos dados
    Run_audit = True


    if Convert_ept_to_csv:
        print("=====>START RF_PARAMETERS CONVERT<=====")
        convert_xls_csv()
        print("=====>FINISH RF_PARAMETERS CONVERT<=====")

    if Compare_ept_columns:
        print("=====>COLUMNS COMPARE<=====")
        compare_df()

    #criando DB (utilizado para recriar db baseado nas planilhas de SP)
    if CreateDb:
        create_db()

    if Upload_csv_to_baco:
        print("=====>START EPT UPLOAD<=====")
        upload_csv(Reset_baco)
        print("=====>FINISH EPT UPLOAD<=====")

    if Run_audit:
        print("=====>START AUDIT RUNNER<=====")
        audit()
        print("=====>FINISH AUDIT RUNNER<=====")

    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'>>>>>> FINISH PROCESS in {delta}<<<<<<<')