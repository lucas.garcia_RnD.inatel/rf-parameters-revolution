select 'sit_name divergente' as audit, case 
	when a.state in ('ac','df','go','ms','mt','ro','to') then 'co'
    when a.state in ('ce','pe','rn','pi','pb','al') then 'ne'
    when a.state in ('sp') then 'sp'
    when a.state in ('pr','sc') then 'pr/sc'
	when a.state in ('rs') then 'rs'
    when a.state in ('rj') then 'rj'
    when a.state in ('es') then 'es'
    else ''
end as regional, a.reg, '4g' as tech, 
a.state as uf, a.sit_acr, '' as cell_id, 'sit_name' as field_name, b.sit_name as field_value
from
(select reg, state, sit_acr, count(1)
from (SELECT reg, state, sit_acr, sit_name FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3
having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_name FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b
on a.state=b.state and a.sit_acr=b.sit_acr
;