#=====================================CELL_NAME DUPLICADO
#=======================LTE
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'cell_name duplicado' as audit, main.reg, 'LTE' as tech, main.state as uf, main.sit_acr, '' as cell_id, 'CELL_NAME' as field_name, main.CELL_NAME as field_value
from
(select  state, reg, SIT_ACR, CELL_NAME, AZIMUTH, count(1) from huawei_ept.RF_LTE
group by state, reg, SIT_ACR, CELL_NAME, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CELL_NAME, AZIMUTH
)as main;
#=======================NR
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'cell_name duplicado' as audit, main.reg, 'NR' as tech, main.state as uf, main.sit_acr, '' as cell_id, 'CELL_NAME' as field_name, main.CELL_NAME as field_value
from
(select  state, reg, SIT_ACR, CELL_NAME, AZIMUTH, count(1) from huawei_ept.RF_NR
group by state, reg, SIT_ACR, CELL_NAME, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CELL_NAME, AZIMUTH
)as main;
#=======================WCDMA
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'cell_name duplicado' as audit, main.reg, 'WCDMA' as tech, main.state as uf, main.sit_acr, '' as cell_id, 'CELL_NAME' as field_name, main.CELL_NAME as field_value
from
(select  state, reg, SIT_ACR, CELL_NAME, AZIMUTH, count(1) from huawei_ept.RF_WCDMA
group by state, reg, SIT_ACR, CELL_NAME, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CELL_NAME, AZIMUTH
)as main;
#=======================GSM
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'cell_name duplicado' as audit, main.reg, 'GSM' as tech, main.state as uf, main.sit_acr, '' as cell_id, 'CELL_NAME' as field_name, main.CELL_NAME as field_value
from
(select  state, reg, SIT_ACR, CELL_NAME, AZIMUTH, count(1) from huawei_ept.RF_GSM
group by state, reg, SIT_ACR, CELL_NAME, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CELL_NAME, AZIMUTH
)as main;

#=====================================CELL_ID DUPLICADO
#=======================LTE
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'CELL_ID duplicado' as audit, main.reg, 'LTE' as tech, main.state as uf, main.sit_acr, main.CELL_ID as cell_id, 'CELL_ID' as field_name, main.CELL_ID as field_value
from
(select  state, reg, SIT_ACR, CELL_ID, AZIMUTH, count(1) from huawei_ept.RF_LTE
group by state, reg, SIT_ACR, CELL_ID, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CELL_ID, AZIMUTH
)as main;
#=======================NR
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'CELL_ID duplicado' as audit, main.reg, 'NR' as tech, main.state as uf, main.sit_acr, '' as cell_id, 'CELL_ID' as field_name, main.CELL_ID as field_value
from
(select  state, reg, SIT_ACR, CELL_ID, AZIMUTH, count(1) from huawei_ept.RF_NR
group by state, reg, SIT_ACR, CELL_ID, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CELL_ID, AZIMUTH
)as main;
#=======================WCDMA
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'CELL_ID duplicado' as audit, main.reg, 'WCDMA' as tech, main.state as uf, main.sit_acr, main.SAC as cell_id, 'SAC' as field_name, main.SAC as field_value
from
(select  state, reg, SIT_ACR, SAC, AZIMUTH, count(1) from huawei_ept.RF_WCDMA
group by state, reg, SIT_ACR, SAC, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, SAC, AZIMUTH
)as main;
#=======================GSM
INSERT INTO huawei_ept.AUDIT_DUPLICADOS
select 'CELL_ID duplicado' as audit, main.reg, 'GSM' as tech, main.state as uf, main.sit_acr, main.CI as cell_id, 'SAC' as field_name, main.CI as field_value
from
(select  state, reg, SIT_ACR, CI, AZIMUTH, count(1) from huawei_ept.RF_GSM
group by state, reg, SIT_ACR, CI, AZIMUTH having (count(1)>1)
order by state, reg, SIT_ACR, CI, AZIMUTH
)as main;