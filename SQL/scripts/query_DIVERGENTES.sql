insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_name divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_name' as field_name, b.sit_name as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_name FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_name FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_name divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_name' as field_name, b.sit_name as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_name FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_name FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_name divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_name' as field_name, b.sit_name as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_name FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_name FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_name divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_name' as field_name, b.sit_name as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_name FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_name FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'longitude divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'longitude' as field_name, b.longitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, longitude FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, longitude FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'longitude divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'longitude' as field_name, b.longitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, longitude FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, longitude FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'longitude divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'longitude' as field_name, b.longitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, longitude FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, longitude FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'longitude divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'longitude' as field_name, b.longitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, longitude FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, longitude FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'latitude divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'latitude' as field_name, b.latitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, latitude FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, latitude FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'latitude divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'latitude' as field_name, b.latitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, latitude FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, latitude FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'latitude divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'latitude' as field_name, b.latitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, latitude FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, latitude FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'latitude divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'latitude' as field_name, b.latitude as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, latitude FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, latitude FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'address divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'address' as field_name, b.address as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, address FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, address FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'address divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'address' as field_name, b.address as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, address FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, address FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'address divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'address' as field_name, b.address as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, address FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, address FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'address divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'address' as field_name, b.address as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, address FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, address FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'cluster divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'cluster' as field_name, b.cluster as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, cluster FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, cluster FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'cluster divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'cluster' as field_name, b.cluster as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, cluster FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, cluster FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'cluster divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'cluster' as field_name, b.cluster as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, cluster FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, cluster FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'cluster divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'cluster' as field_name, b.cluster as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, cluster FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, cluster FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'city divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'city' as field_name, b.city as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, city FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, city FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'city divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'city' as field_name, b.city as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, city FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, city FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'city divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'city' as field_name, b.city as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, city FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, city FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'city divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'city' as field_name, b.city as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, city FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, city FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NEIGHBORHOOD divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NEIGHBORHOOD' as field_name, b.NEIGHBORHOOD as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NEIGHBORHOOD divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NEIGHBORHOOD' as field_name, b.NEIGHBORHOOD as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NEIGHBORHOOD divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NEIGHBORHOOD' as field_name, b.NEIGHBORHOOD as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NEIGHBORHOOD divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NEIGHBORHOOD' as field_name, b.NEIGHBORHOOD as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NEIGHBORHOOD FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'ZIPCODE divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'ZIPCODE' as field_name, b.ZIPCODE as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, ZIPCODE FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, ZIPCODE FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'ZIPCODE divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'ZIPCODE' as field_name, b.ZIPCODE as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, ZIPCODE FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, ZIPCODE FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'ZIPCODE divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'ZIPCODE' as field_name, b.ZIPCODE as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, ZIPCODE FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, ZIPCODE FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'ZIPCODE divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'ZIPCODE' as field_name, b.ZIPCODE as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, ZIPCODE FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, ZIPCODE FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'area divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'area' as field_name, b.area as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, area FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, area FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'area divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'area' as field_name, b.area as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, area FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, area FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'area divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'area' as field_name, b.area as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, area FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, area FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'area divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'area' as field_name, b.area as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, area FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, area FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'lac divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'lac' as field_name, b.lac as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, lac FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, lac FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'lac divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'lac' as field_name, b.lac as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, lac FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, lac FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;

insert into huawei_ept.AUDIT_DIVERGENTES
select 'gnodeb__tac divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'gnodeb__tac' as field_name, b.gnodeb__tac as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, gnodeb__tac FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, gnodeb__tac FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'rac divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'rac' as field_name, b.rac as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, rac FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, rac FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'rac divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'rac' as field_name, b.rac as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, rac FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, rac FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;

insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_type divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_type' as field_name, b.sit_type as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_type FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_type FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_type divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_type' as field_name, b.sit_type as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_type FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_type FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_type divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_type' as field_name, b.sit_type as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_type FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_type FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'sit_type divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'sit_type' as field_name, b.sit_type as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, sit_type FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, sit_type FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'site_estrutura divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'site_estrutura' as field_name, b.site_estrutura as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, site_estrutura FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, site_estrutura FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'site_estrutura divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'site_estrutura' as field_name, b.site_estrutura as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, site_estrutura FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, site_estrutura FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'site_estrutura divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'site_estrutura' as field_name, b.site_estrutura as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, site_estrutura FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, site_estrutura FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'site_estrutura divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'site_estrutura' as field_name, b.site_estrutura as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, site_estrutura FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, site_estrutura FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NE_NAME divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NE_NAME' as field_name, b.NE_NAME as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NE_NAME FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NE_NAME FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NE_NAME divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NE_NAME' as field_name, b.NE_NAME as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NE_NAME FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NE_NAME FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NE_NAME divergente' as audit, a.reg, 'LTE' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NE_NAME' as field_name, b.NE_NAME as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NE_NAME FROM huawei_ept.RF_LTE group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NE_NAME FROM huawei_ept.RF_LTE group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'NE_NAME divergente' as audit, a.reg, 'NR' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'NE_NAME' as field_name, b.NE_NAME as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, NE_NAME FROM huawei_ept.RF_NR group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, NE_NAME FROM huawei_ept.RF_NR group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'mgc divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'mgc' as field_name, b.mgc as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, mgc FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, mgc FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'mgc divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'mgc' as field_name, b.mgc as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, mgc FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, mgc FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;

insert into huawei_ept.AUDIT_DIVERGENTES
select 'mgw divergente' as audit, a.reg, 'GSM' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'mgw' as field_name, b.mgw as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, mgw FROM huawei_ept.RF_GSM group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, mgw FROM huawei_ept.RF_GSM group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;
insert into huawei_ept.AUDIT_DIVERGENTES
select 'mgw divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'mgw' as field_name, b.mgw as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, mgw FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, mgw FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;

insert into huawei_ept.AUDIT_DIVERGENTES
select 'ura divergente' as audit, a.reg, 'WCDMA' as tech,a.state as uf, a.sit_acr, '' as cell_id, 'ura' as field_name, b.ura as field_value from
(select reg, state, sit_acr, count(1)from (SELECT reg, state, sit_acr, ura FROM huawei_ept.RF_WCDMA group by 1,2,3,4 order by 1,2,3,4) as c
group by 1,2,3 having count(1)>1) as a
left join
(SELECT state, sit_acr, ura FROM huawei_ept.RF_WCDMA group by 1,2,3 order by 1,2,3) as b on a.state=b.state and a.sit_acr=b.sit_acr;